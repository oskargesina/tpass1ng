import { PasswordGroupService } from './../../security/services/password-group.service';
import { SharedService } from 'src/app/shared/shared.service';
import { SgUrl } from 'saigon-lib';
import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
})
export class TreeComponent implements OnInit {
  @Input() branchList: any;
  @Input() level: any = undefined;
  @Input() parentList: any = undefined;

  public SgUrl = SgUrl;

  constructor(
    public ss: SharedService,
    private router: Router,
    private passwordGroupService: PasswordGroupService,
  ) { }

  ngOnInit() {}
  
  public getDetails(branch) {
    // jesli jest parent, przekierowywujemy na detail
    if (branch.level == 0) {
      SgUrl.navigate(this.router, 'passwordGroupDetail', {groupId: branch.id})

      // po ponownym kliknieciu na primary, zamykamy submenu
      this.ss.setActiveGroup({});

    } else {
      
      SgUrl.navigate(this.router, 'passwordSubGroupDetail', {groupId: this.ss.activeParentGroup['id'], subGroupId: branch.id})
      
      // ustawiamy active group
      this.ss.setActiveGroup(branch);

      // robimy collapse jesli ma dzieci
      if (this.passwordGroupService.getChildren(branch.id).length) this.collapseParent(branch.id);

    }
  }

  public isActive(branch) {
    let result;

    if (branch.level == 0) {
      result = branch.id == this.ss.activeParentGroup['id'];
    } else {
      result = branch.id == this.ss.activeGroup['id'];
    }

    return result;
  }

  private collapseParent(branchId, collapseOnly?) {
    if (this.isParentInList(branchId, this.parentList)) {
      
      // usuwamy element z listy
      this.parentList.splice(this.getParentInListIndex(branchId, this.parentList), 1);

      // usuwamy jego dzieci
      let array = this.passwordGroupService.getChildren(branchId);

      for (var j = 0; j < array.length; j++) {
        this.collapseParent(array[j]['id'], true);
      }

    } else {
      if (collapseOnly != true) {
        this.parentList.push(branchId);
      }

    }    
  }

  private getParentInListIndex(parent, parentList) {
    return parentList.indexOf(parent);
  }

  private isParentInList(parent, parentList) {
    return this.getParentInListIndex(parent, parentList) != -1
  }

  public showBranch(level, parent) {
    let levelResult;
    let parentResult;
    
    if (this.level == undefined) {
      levelResult = true;
    } else {
      levelResult = level == this.level;
    }

    if (this.parentList == undefined) {
      parentResult = true;
    } else {
      parentResult = this.isParentInList(parent, this.parentList);
    }

    return (levelResult && parentResult);
  }

  public calcualtePadding(level, isPrimary) {
    let result = 0;
    if (isPrimary == false) level = level - 1;
    if (level > 0) result = (level * 16);

    return result + "px";
  }

}
