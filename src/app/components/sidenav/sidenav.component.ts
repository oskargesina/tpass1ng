import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { SgUrl } from 'saigon-lib';
import { LogoutService } from 'src/app/accounts/logout/logout.service';
import { SharedService } from './../../shared/shared.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  public SgUrl = SgUrl;

  constructor(
    public ss: SharedService,
    private logoutService: LogoutService,
    private cookieService: CookieService,
    private router: Router
    ) {}

  ngOnInit() {}

  logout() {
    this.logoutService.logout().subscribe(
      next => {
        if(this.cookieService.check('authToken')) this.cookieService.delete('authToken')
        this.router.navigateByUrl('login')
      },
      error => {
        console.log(error)
      }
    )
  }

}
