import { SgUrl } from 'saigon-lib';
import { SharedService } from 'src/app/shared/shared.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub-sidenav',
  templateUrl: './sub-sidenav.component.html',
  styleUrls: ['./sub-sidenav.component.scss']
})
export class SubSidenavComponent implements OnInit {
  public SgUrl = SgUrl;

  constructor(
    public ss: SharedService,
  ) { }

  ngOnInit() {}

  public getParentList() {

    let result = [];
    if (this.ss.activeParentGroup) {
      result = [this.ss.activeParentGroup['id']];
    }

    return result;
  }

}
