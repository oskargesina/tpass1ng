import { SharedService } from 'src/app/shared/shared.service';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { UserPassword } from './../classes/user-password';
import { Component, OnInit } from '@angular/core';
import { UserPasswordService } from '../services/user-password.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordGroup } from './../classes/password-group';
import { PasswordGroupService } from './../services/password-group.service';
import { SgUrl } from 'saigon-lib';

@Component({
  selector: 'app-password-group-create',
  templateUrl: './password-group-create.component.html',
  styleUrls: ['./password-group-create.component.scss']
})
export class PasswordGroupCreateComponent implements OnInit {
  passwordGroup = {} as PasswordGroup
  passwordGroupForm: FormGroup;
  public SgUrl = SgUrl;
  public group;

  constructor(
    private formBuilder: FormBuilder,
    private passwordGroupService: PasswordGroupService,
    private ss: SharedService,
    private route: ActivatedRoute,
    private router: Router
    
  ) {
    this.route.paramMap.subscribe(params => {
      if (params.get('groupId')) {
        this.ss.setActiveParentGroup(passwordGroupService.getGroup(params.get('groupId')));
        this.group = this.ss.activeParentGroup; 

      } else if (params.get('subGroupId')) {
          this.ss.setActiveGroup(passwordGroupService.getGroup(params.get('subGroupId')));
          this.group = this.ss.activeGroup; 
      }
      
      
    });  
  
  }

  ngOnInit() {
    this.passwordGroupForm = this.formBuilder.group({
      name: [this.passwordGroup.name],
    });    
  }

  save() {
    let f = this.passwordGroupForm;
    let data = {...f.value};

    if (this.group) {
      data.parent = this.group['id'];
    }
    
    this.passwordGroupService.create(data).subscribe(
      result => {
        this.ss.passwordGroup.push(result);
        this.passwordGroupForm.reset();

        this.SgUrl.navigate(this.router, 'passwordGroupDetail', {groupId: result['id']});

      },
      error => {
        console.log(data)
        console.log(error)
      }
    )
  }  

}
