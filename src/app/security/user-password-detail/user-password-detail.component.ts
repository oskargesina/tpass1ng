import { SgUrl } from 'saigon-lib';
import { SharedService } from './../../shared/shared.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordGroupService } from '../services/password-group.service';
import { CryptoService } from 'src/app/crypto/crypto.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';


@Component({
  selector: 'app-user-password-detail',
  templateUrl: './user-password-detail.component.html',
  styleUrls: ['./user-password-detail.component.scss']
})
export class UserPasswordDetailComponent implements OnInit {
  public SgUrl = SgUrl;
  public userPassword = {};
  public group = {};
  public userId;
  public secretField;
  public actionName = "Show password";
  public secretIsVisible = false;
  public decryptedPassword = {};
  passId = this.route.snapshot.paramMap.get("passwordId");
  public users = {}
  public users$: Observable<any>;
  sharePassForm: FormGroup;

  
  constructor(
    public ss: SharedService,
    private route: ActivatedRoute,
    private router: Router,
    private passwordGroupService: PasswordGroupService,
    private crypto: CryptoService,
    private fb: FormBuilder
  ) {
    this.route.paramMap.subscribe(params => {
      this.ss.setActiveParentGroup(passwordGroupService.getGroup(params.get('groupId')));
      this.group = this.ss.activeParentGroup; 
      
      if (params.get('subGroupId')) {
        this.ss.setActiveGroup(passwordGroupService.getGroup(params.get('subGroupId')))
        this.group = this.ss.activeGroup; 
      }

      this.userPassword = this.group['password_objects'].find(password => password.id == params.get('passwordId'))


      this.sharePassForm = this.fb.group({
        user: []
      })
  
      this.users$ = this.ss.getUsers().pipe(
        tap(next => {
           this.users = next 
          }), shareReplay());

    });    

    this.ss.getUser().subscribe(
      result => {
        this.userId = result["user_id"];
        var privateKey = ss.getLocalStorage(`privateKeyUserId${this.userId}`);
        for(let key in this.userPassword) {

          if(key != 'secret') {
            var decryptedField = crypto.decryptPass(privateKey, this.userPassword[key])

            console.log("decryptedField");
            console.log(key);
            console.log(decryptedField);

            this.decryptedPassword[key] = decryptedField;
          }

        }

        this.secretField = this.userPassword['secret'];
          
      },
      error => {
        console.log(error)
      }
    )
  }

  ngOnInit() {
  }

  toggleSecret() {
    this.secretIsVisible = !this.secretIsVisible;

    if(this.secretIsVisible) {
      var privateKey = this.ss.getLocalStorage(`privateKeyUserId${this.userId}`);
      this.secretField = this.crypto.decryptPass(privateKey, this.userPassword['secret'])
      this.actionName = "Hide password"
    } else {
      this.secretField = this.userPassword['secret']
      this.actionName = "Show password"
    }
  }


  sharePass() {
    console.log(this.passId)
    let f = this.sharePassForm;
    let data = {...f.value}
    data['role'] = 10
    data['url'] = ''
    var privateKey = this.ss.getLocalStorage(`privateKeyUserId${this.userId}`);
    data['secret'] = this.crypto.decryptPass(privateKey, this.userPassword['secret'])
    data['title'] = this.crypto.decryptPass(privateKey, this.userPassword['title'])
    // data['user_name'] = this.crypto.decryptPass(privateKey, this.userPassword['username'])
    data['user_name'] = ''
    data['notes'] = this.crypto.decryptPass(privateKey, this.userPassword['notes'])
    console.log(data)

    this.ss.sharePassword(this.passId, data).subscribe(next => {
      console.log(next)
    },
    error => {
      console.log(error)
    })
  }

}
