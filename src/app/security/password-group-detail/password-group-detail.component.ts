import { PasswordGroupService } from './../services/password-group.service';
import { SgUrl } from 'saigon-lib';
import { SharedService } from './../../shared/shared.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CryptoService } from 'src/app/crypto/crypto.service';

@Component({
  selector: 'app-password-group-detail',
  templateUrl: './password-group-detail.component.html',
  styleUrls: ['./password-group-detail.component.scss']
})
export class PasswordGroupDetailComponent implements OnInit {
  public SgUrl = SgUrl;
  public group = {};
  public userId: string;
  
  constructor(
    public ss: SharedService,
    private route: ActivatedRoute,
    private passwordGroupService: PasswordGroupService,
    private crypto: CryptoService
  ) {
    this.route.paramMap.subscribe(params => {
      this.ss.setActiveParentGroup(passwordGroupService.getGroup(params.get('groupId')));
      this.group = this.ss.activeParentGroup; 
      
      if (params.get('subGroupId')) {
        this.ss.setActiveGroup(passwordGroupService.getGroup(params.get('subGroupId')))
        this.group = this.ss.activeGroup; 
      }
    });

    this.ss.getUser().subscribe(
      result => {
        this.userId = result["user_id"];
      },
      error => {
        console.log(error)
      }
    )

  }

  ngOnInit() {
  }

  decrypt(encrypted) {
    var privateKey = this.ss.getLocalStorage(`privateKeyUserId${this.userId}`);
    var decrypted = this.crypto.decryptPass(privateKey, encrypted)
    console.log("tutaj");
    console.log(encrypted);
    console.log(decrypted);
    return decrypted;
  }

}
