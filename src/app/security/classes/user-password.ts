export interface UserPassword {
  title: string, 
  username: string
  secret: string
  repeatSecret: string
  notes: string
}