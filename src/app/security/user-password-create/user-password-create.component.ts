import { PasswordGroupService } from './../services/password-group.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SgUrl } from 'saigon-lib';
import { SharedService } from 'src/app/shared/shared.service';
import { UserPasswordService } from '../services/user-password.service';
import { UserPassword } from './../classes/user-password';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-password-create',
  templateUrl: './user-password-create.component.html',
  styleUrls: ['./user-password-create.component.scss']
})
export class UserPasswordCreateComponent implements OnInit {
  userPassword = {} as UserPassword
  userPasswordForm: FormGroup;
  public SgUrl = SgUrl;

  constructor(
    private formBuilder: FormBuilder,
    private userPasswordService: UserPasswordService,
    private passwordGroupService: PasswordGroupService,
    private ss: SharedService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    // set active group
    this.route.paramMap.subscribe(params => {
      this.ss.setActiveParentGroup(passwordGroupService.getGroup(params.get('groupId')));
      
      // if (params.get('subGroupId')) {
      //   this.ss.setActiveGroup(passwordGroupService.getGroup(params.get('subGroupId')))
      // }      
    });    
  }

  ngOnInit() {
    this.userPasswordForm = this.formBuilder.group({
      title: [this.userPassword.title],
      username: [this.userPassword.username],
      secret: [this.userPassword.secret],
      repeatSecret: [this.userPassword.repeatSecret],
      notes: [this.userPassword.notes],
    });    
  }

  back() {
    if (this.ss.activeGroup) {
      this.SgUrl.navigate(this.router, 'passwordSubGroupDetail', {groupId: this.ss.activeParentGroup['id'], subGroupId: this.ss.activeGroup['id']});
    } else {
      this.SgUrl.navigate(this.router, 'passwordGroupDetail', {groupId: this.ss.activeParentGroup['id']});
    }
  }

  save() {
    let f = this.userPasswordForm;
    let data = {...f.value};
    
    data.group = this.ss.activeGroup['id'] ? this.ss.activeGroup['id'] : this.ss.activeParentGroup['id'];

    this.userPasswordService.create(data).subscribe(
      result => {
        let group;
        if (this.ss.activeGroup['id']) {
          group = this.ss.activeGroup;
        } else {
          group = this.ss.activeParentGroup;
        }
        group['password_objects'].push(result);
        this.back();
      },
      error => {
        console.log(data)
        console.log(error)
      }
    )
  }  

}
