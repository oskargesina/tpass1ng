import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SgUtil } from 'saigon-lib';

@Injectable({
  providedIn: 'root'
})
export class UserPasswordService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.url;

  create(data) {
    let url = SgUtil.url(this.apiUrl, 'passwords');
    return this.http.post(url, data);
  }
}