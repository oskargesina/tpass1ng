import { SharedService } from 'src/app/shared/shared.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SgUtil } from 'saigon-lib';

@Injectable({
  providedIn: 'root'
})
export class PasswordGroupService {
  constructor(
    private http: HttpClient,
    private ss: SharedService,
  ) {}

  apiUrl = environment.url;

  list(params?) {
    let url = SgUtil.url(this.apiUrl, 'groups');
    return this.http.get(url, params);
  }

  create(params?) {
    let url = SgUtil.url(this.apiUrl, 'groups');
    return this.http.post(url, params);
  }

  loadPasswordGroups() {
    let authToken = this.ss.getCookie('authToken');

    if (authToken) {
      this.loadUserData()
      return new Promise((resolve, reject) => {

        return this.list().subscribe(result => {
          this.ss.passwordGroup = result['password_group'];

          let passwordList = [];
          for (let i in result['shared_passwords']) {
            passwordList.push(result['shared_passwords'][i]['password_object']);
          }

          this.ss.passwordGroup.unshift({
            id: 'shared',
            level: 0,
            name: "Shared for you",
            password_objects: passwordList,        
          })

          this.ss.sharedPasswordGroup = result['shared_password_group'];
          this.ss.sharedPasswords = result['shared_passwords'];          

          resolve(true)
        }),
        error => {
          if (error.status == 400) {
            this.ss.removeCookie('authToken');
            reject(error);
          }
        }

      })
    }
  }
  
  getGroup(groupId) {
    return this.ss.passwordGroup.find(group => group.id == groupId)
  }
  
  getChildren(parentId) {
    return this.ss.passwordGroup.filter(group => group.parent == parentId)
  }

  loadUserData() {
    if (this.ss.user != undefined && this.ss.user.constructor === Object) {
      this.ss.getUser().subscribe(result => {
        this.ss.user = result
      })
    }
  }
}