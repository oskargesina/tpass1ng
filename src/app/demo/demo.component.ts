import { Component } from '@angular/core';
import { CryptoService } from '../crypto/crypto.service';
import * as JsEncryptModule from 'jsencrypt';

@Component({
  selector: "demo",
  templateUrl: "demo.component.html"
})
export class DemoComponent {
  constructor(private crypto: CryptoService) {}
  
  encrypt;
  decrypt;


  privateKey;
  publicKey;
  encryptedPass;
  decryptedPass;

  ngOnChanges() {
    this.encrypt = new JsEncryptModule.JSEncrypt();
    this.decrypt = new JsEncryptModule.JSEncrypt();
  }
  
  public setKeys() {
    this.decrypt.setPrivateKey(this.privateKey);
    this.encrypt.setPublicKey(this.publicKey);

  }
  public encryptPass() {
    this.encryptedPass = this.encrypt.encrypt(this.decryptedPass);
    console.log(this.encryptedPass);
  }
  public decryptPass() {
    this.decrypt.decryp(this.encryptedPass);
    console.log(this.decryptedPass);
    
  }

  public enctryptPassConsole() {
    this.crypto.encryptPass();
  }

  public dectryptPassConsole() {
    // this.crypto.decryptPass();
  }

}