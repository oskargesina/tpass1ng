import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { SgUrl } from 'saigon-lib';
import { SharedService } from '../shared/shared.service';
import { LogoutService } from '../accounts/logout/logout.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userId: string | Number;
  public SgUrl = SgUrl

  constructor(private router: Router,
    private cookieService: CookieService,
    private ss: SharedService,
    private logoutService: LogoutService) {
     }

  isLoggedin(): boolean {
    if(this.cookieService.check('authToken')) {
      // && this.ss.getLocalStorage(`privateKeyUserId${this.ss.user['user_id']}`)
      return true
    }
    // @PS to trzeba udroznic.
    // else if (!this.ss.getLocalStorage(`privateKeyUserId${this.ss.user['user_id']}`)) {
    //   this.logoutService.logout().subscribe(() => {})
    //   this.cookieService.delete('authToken')
    //   this.router.navigateByUrl('login')
    //   return false
    // }
    else {
      this.router.navigateByUrl('login')
      return false
    }
  }

  hasPrivKey(): boolean {
    if(this.cookieService.check('authToken')) {
      this.ss.getUser().subscribe(result => {
        if(result['user_id']) {
          if (this.ss.getLocalStorage(`privateKeyUserId${result['user_id']}`) && this.ss.getLocalStorage(`privateKeyUserId${result['user_id']}`) != undefined) {
            return true
          }
          else {
            this.router.navigateByUrl('login-private')
          }
        }
      })
      return true
    }
    else {
      return false
    }
  }

}
