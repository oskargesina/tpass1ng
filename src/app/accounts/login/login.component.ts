import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login.service';
import { Login } from './classes/login'
import { FormBuilder, FormGroup } from "@angular/forms";
import { SharedService } from 'src/app/shared/shared.service';
import { SgUrl } from 'saigon-lib';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login = {} as Login
  loginForm: FormGroup;

  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private ss: SharedService,
    private router: Router,
    private cookieService: CookieService) {
      // hide sidenav
      this.ss.hideSidenav = true;
    }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: [this.login.email],
      password: [this.login.password]
    });
  }

  checkPrivKey(): void {
    if(this.ss.getLocalStorage('privKey') || this.ss.getLocalStorage('privKey') != undefined) {
    SgUrl.navigate(this.router, 'dashboard')
    }
    else {
      SgUrl.navigate(this.router, 'loginPrivKey')
    }
  }

  logIn() {
    let f = this.loginForm;
    let data = {...f.value}
    this.loginService.login(data).subscribe(
      result => {
        if(this.cookieService.check('authToken')) {
          this.cookieService.delete('authToken')
          this.cookieService.set('authToken', result['token'], 1/24)
          this.checkPrivKey()
        }
        else {
          this.cookieService.set('authToken', result['token'], 1/24)
          this.checkPrivKey()
        }
      },
      error => {
        console.log(error)
      }
    )
  }

}
