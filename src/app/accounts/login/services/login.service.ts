import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SgUtil } from 'saigon-lib';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.url;

  login(data: Object) {
    let url = SgUtil.url(this.apiUrl, 'users', 'login');
    return this.http.post(url, data);
  }
}

