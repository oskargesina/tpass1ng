export interface Registration {
  email: string, 
  password1: string,
  password2: string,
  first_name: string,
  last_name: string,
}