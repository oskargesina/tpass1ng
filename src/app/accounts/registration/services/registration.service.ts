import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SgUtil } from 'saigon-lib';


@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.url;

  register(data: Object) {
    let url = SgUtil.url(this.apiUrl, 'users', 'register');
    return this.http.post(url, data);
  }
}

