import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { SharedService } from 'src/app/shared/shared.service';
import { SgUrl } from 'saigon-lib';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { RegistrationService } from './services/registration.service';
import { Registration } from './classes/registration';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registration = {} as Registration
  registrationForm: FormGroup;

  constructor(
    private registrationService: RegistrationService,
    private formBuilder: FormBuilder,
    private ss: SharedService,
    private router: Router,
    private cookieService: CookieService) {
      // hide sidenav
      this.ss.hideSidenav = true;
    }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      email: [this.registration.email],
      password1: [this.registration.password1],
      password2: [this.registration.password2],
      first_name: [this.registration.first_name],
      last_name: [this.registration.last_name],
    });
    
  }

  register() {
    let f = this.registrationForm;
    let data = {...f.value}
    this.registrationService.register(data).subscribe(
      result => {
        this.ss.privateKey = result['private_key']
        this.cookieService.set('authToken', result['token'], 1/24)
        if(this.ss.getLocalStorage('privKey') != undefined) {
          this.router.navigateByUrl('')
        }
        else {
          this.router.navigateByUrl('login-private')
        }
      },
      error => {
        console.log(error)
      }
    )
  }

}
