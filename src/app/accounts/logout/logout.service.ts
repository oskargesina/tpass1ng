import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SgUtil } from 'saigon-lib';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.url;

  logout() {
    let url = SgUtil.url(this.apiUrl, 'users', 'logout');
    return this.http.post(url, {});
  }
}
