import { SharedService } from 'src/app/shared/shared.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { User } from './classes/login-private-key';

@Component({
  selector: 'app-login-private-key',
  templateUrl: './login-private-key.component.html',
  styleUrls: ['./login-private-key.component.scss']
})
export class LoginPrivateKeyComponent implements OnInit {

  constructor(private ss: SharedService, private formBuilder: FormBuilder, private router: Router) {
    this.ss.hideSidenav = true;
   }

  userPrivateKeyForm: FormGroup;

  user = {} as User;

  ngOnInit() {
    // this.ss.privateKey
    this.userPrivateKeyForm = this.formBuilder.group({
      privateKey: [this.ss.privateKey],
    });



    this.ss.getUser().subscribe(
      result => {

        this.user.id = result["user_id"]
        this.user.firstName = result["first_name"]
        this.user.lastName = result["last_name"]

        console.log(this.user)
        
      },
      error => {
        console.log(error)
      }
    )
  }

  save() {
    var privateKey = this.userPrivateKeyForm.value.privateKey;
    this.ss.privateKey = null;
    this.ss.setLocalStorage('privateKeyUserId' + this.user.id, privateKey);
    this.router.navigateByUrl('');
  }

}
