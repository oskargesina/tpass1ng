import { PasswordGroupService } from './../security/services/password-group.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private ss: SharedService,
    private passwordGroupService: PasswordGroupService,
  ) { 
    // hide sidenav
    this.ss.hideSidenav = false;

    // load passwords
    if (!this.ss.passwordGroup) {
      this.passwordGroupService.loadPasswordGroups();
    }
  }

  ngOnInit() {
  }

}
