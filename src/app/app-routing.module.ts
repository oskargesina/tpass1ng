import { PasswordGroupDetailComponent } from './security/password-group-detail/password-group-detail.component';
import { PasswordGroupService } from './security/services/password-group.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPrivateKeyComponent } from './accounts/login-private-key/login-private-key.component';
import { LoginComponent } from './accounts/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoComponent } from './demo/demo.component';
import { UserPasswordCreateComponent } from './security/user-password-create/user-password-create.component';
import { SgUrl } from 'saigon-lib';
import { RegistrationComponent } from './accounts/registration/registration.component';
import { PasswordGroupCreateComponent } from './security/password-group-create/password-group-create.component';
import { UserPasswordDetailComponent } from './security/user-password-detail/user-password-detail.component';
import { AuthPriv } from './auth/auth-priv.guard';


const routes: Routes = [
  { path: '', component: DashboardComponent, data: { routeName: "dasboard" }, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent, data: { routeName: "login" } },
  { path: 'registration', component: RegistrationComponent, data: { routeName: "registration" } },
  { path: 'demo', component: DemoComponent, data: { routeName: "demo" } },
  { path: 'login-private', component: LoginPrivateKeyComponent, data: { routeName: "loginPrivKey" }, canActivate: [AuthPriv] },
  
  // password group
  { path: 'new-group', component: PasswordGroupCreateComponent, data: { routeName: "passwordGroupCreate" }, canActivate: [AuthGuard] },
  { path: 'new-group/:groupId', component: PasswordGroupCreateComponent, data: { routeName: "passwordGroupCreate" }, canActivate: [AuthGuard] },
  { path: 'new-group/:groupId/:subGroupId', component: PasswordGroupCreateComponent, data: { routeName: "passwordGroupCreate" }, canActivate: [AuthGuard] },
  { path: ':groupId', component: PasswordGroupDetailComponent, data: { routeName: "passwordGroupDetail" }, canActivate: [AuthGuard] },
  
  // user password
  { path: ':groupId/p/:passwordId', component: UserPasswordDetailComponent, data: { routeName: "userPasswordDetail" }, canActivate: [AuthGuard] },
  { path: ':groupId/p/:passwordId/:subGroupId', component: UserPasswordDetailComponent, data: { routeName: "userPasswordDetail" }, canActivate: [AuthGuard] },

  { path: ':groupId/s/:subGroupId', component: PasswordGroupDetailComponent, data: { routeName: "passwordSubGroupDetail" }, canActivate: [AuthGuard] },
  { path: ':groupId/new-password', component: UserPasswordCreateComponent, data: { routeName: "userPasswordCreate" }, canActivate: [AuthGuard] },

];

SgUrl.init(routes);
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
