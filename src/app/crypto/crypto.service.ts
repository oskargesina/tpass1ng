import { Injectable } from '@angular/core';
import * as JsEncryptModule from 'jsencrypt';
// import { Buffer } from 'buffer';


@Injectable()
export class CryptoService {


  encryptPass() {
    const encrypt = new JsEncryptModule.JSEncrypt();
    console.log(encrypt);

    encrypt.setPublicKey();

    // this.result = encrypt.encrypt("działa?????:D");

  }

  decryptPass(privateKey: string, encrypted: string) {
    const decrypt = new JsEncryptModule.JSEncrypt();
    decrypt.setPrivateKey(privateKey);
    const decrypted = decrypt.decrypt(encrypted);

    return decrypted;

  }

}