import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_LABEL_GLOBAL_OPTIONS } from '@angular/material/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
import { SaigonLibModule, SgUrl } from 'saigon-lib';
import { LoginPrivateKeyComponent } from './accounts/login-private-key/login-private-key.component';
import { LoginComponent } from './accounts/login/login.component';
import { RegistrationComponent } from './accounts/registration/registration.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { TreeComponent } from './components/tree/tree.component';
import { CryptoService } from './crypto/crypto.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoComponent } from './demo/demo.component';
import { PasswordGroupDetailComponent } from './security/password-group-detail/password-group-detail.component';
import { PasswordGroupService } from './security/services/password-group.service';
import { UserPasswordCreateComponent } from './security/user-password-create/user-password-create.component';
import { SharedService } from './shared/shared.service';
import { HttpAuthClient } from './utils/http-auth-client';
import { PasswordGroupCreateComponent } from './security/password-group-create/password-group-create.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { UserPasswordDetailComponent } from './security/user-password-detail/user-password-detail.component';
import { SubSidenavComponent } from './components/sub-sidenav/sub-sidenav.component';
import {MatSelectModule} from '@angular/material/select';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DemoComponent,
    DashboardComponent,
    TreeComponent,
    LoginPrivateKeyComponent,
    UserPasswordCreateComponent,
    SidenavComponent,
    SearchComponent,
    PasswordGroupDetailComponent,
    RegistrationComponent,
    PasswordGroupCreateComponent,
    UserPasswordDetailComponent,
    SubSidenavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SaigonLibModule,
    BrowserAnimationsModule,
    MatInputModule,
    PerfectScrollbarModule,
    MatSelectModule
  ],
  providers: [
    SgUrl,
    SharedService, CookieService, CryptoService,
    
    {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}},
    {provide: HTTP_INTERCEPTORS, useClass: HttpAuthClient, multi: true},
    {provide: APP_INITIALIZER, useFactory: groupPasswordProvider, deps: [PasswordGroupService], multi: true},
    {provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG}    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function groupPasswordProvider(provider: PasswordGroupService) {
  return () => provider.loadPasswordGroups()
}