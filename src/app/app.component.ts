import { SharedService } from 'src/app/shared/shared.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tpass1ng';

  constructor(
    public ss: SharedService
  ) {
    
  }
}
