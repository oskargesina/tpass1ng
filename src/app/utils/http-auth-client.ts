import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class HttpAuthClient implements HttpInterceptor {
  constructor(public router: Router, private cookieService: CookieService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.cookieService.check('authToken')) {
      req = req.clone({
        setHeaders: {
          'Authorization': this.cookieService.get('authToken')
        }
      })
      return next.handle(req)
    }
    else {
      return next.handle(req)
    }
  }
}
