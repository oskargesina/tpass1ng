import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { SgUtil } from 'saigon-lib';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public hideSidenav: boolean = false;

  // password groups
  public activeParentGroup = {};
  public activeGroup = {};

  public passwordGroup;
  public sharedPasswordGroup;
  public sharedPasswords;
  public user = {}
  public privateKey;

  constructor(private http: HttpClient) { }

  apiUrl = environment.url;

  setActiveGroup(groupId) {
    this.activeGroup = groupId;
  }

  setActiveParentGroup(groupId) {
    this.activeParentGroup = groupId;
  }

  getCookie(name: string) {
    let v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
  }

  removeCookie(name: string): void {
    let expires = moment().add(-1, 'd').toDate();
    document.cookie = `${name}='';expires=${expires}`;
  }

  setLocalStorage(name: string, value: string): void {
    window.localStorage.setItem(name, value)
  }

  getLocalStorage(name: string) {
    return window.localStorage.getItem(name)
  }

  getUser() {
    let url = SgUtil.url(this.apiUrl, 'users', 'get-user');
    return this.http.get(url);
  }

  getUsers() {
    let url = SgUtil.url(this.apiUrl, 'users');
    return this.http.get(url);
  }

  sharePassword(id, data: Object) {
    let url = SgUtil.url(this.apiUrl, 'passwords', id, 'share');
    return this.http.post(url, data);
  }
}
