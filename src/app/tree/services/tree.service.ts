import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SgUtil } from 'saigon-lib';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TreeService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.url;

  getTreeData() {
    let url = SgUtil.url(this.apiUrl, 'tree');
    return this.http.get(url);
  }
}
