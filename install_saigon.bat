SET project_name=tpass1ng

cd ../saigon_lib
IF EXIST node_modules (
    ng build saigon-lib && cd dist\saigon-lib && npm pack && for %%a in (*.tgz) do cd ../../../%project_name% && npm install ..\saigon_lib\dist\saigon-lib\%%a
) ELSE (
    npm i && ng build saigon-lib && cd dist\saigon-lib && npm pack && for %%a in (*.tgz) do cd ../../../%project_name% && npm install ..\saigon_lib\dist\saigon-lib\%%a
)